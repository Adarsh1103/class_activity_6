package com.example.class_activity_6;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private ListView studentListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        studentListView = findViewById(R.id.studentd_list_view);
        new GetStudents().execute();
    }

    private void addDataToList(HashMap<Integer, Student> map) {
        StudentAdapter student_adapter = new StudentAdapter(map);
        studentListView.setAdapter(student_adapter);
        ((BaseAdapter) studentListView.getAdapter()).notifyDataSetChanged();
    }

    public String initiateApiCall(String reqUrl) {
        String response = null;
        try {
            URL url = new URL(reqUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = parseStream(in);
        } catch (Exception e) {
        }
        return response;
    }

    private String parseStream(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null)
                sb.append(line).append('\n');
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    private class GetStudents extends AsyncTask<Void, Void, HashMap<Integer, Student>> {

        @Override
        protected void onPostExecute(HashMap<Integer, Student> map) {
            super.onPostExecute(map);
            addDataToList(map);
        }

        @Override
        protected HashMap<Integer, Student> doInBackground(Void... arg0) {
            String url = "https://run.mocky.io/v3/ade12cda-3edb-44bb-b3e1-129166060de2";
            String jsonStr = initiateApiCall(url);
            HashMap<Integer, Student> map = new HashMap<>();
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONArray students = jsonObj.getJSONArray("Students");

                    for (int i = 0; i < students.length(); i++) {
                        Student s = new Student();
                        s.setFirstName(students.getJSONObject(i).getString("First name"));
                        s.setLastName(students.getJSONObject(i).getString("Last name"));
                        s.setAge(students.getJSONObject(i).getString("age"));
                        s.setEnrollmentNo(students.getJSONObject(i).getString("enrollment number"));
                        s.setClassC(students.getJSONObject(i).getString("Class"));
                        map.put(i, s);
                    }

                } catch (final JSONException e) {
                    System.out.println(e.getMessage());
                    runOnUiThread(() -> Toast.makeText(MainActivity.this, "Json parsing error: " + e.getMessage(), Toast.LENGTH_LONG).show());

                }
                return map;

            } else {
                System.out.println("Couldn't get json from server.");
                runOnUiThread(() -> Toast.makeText(getApplicationContext(),
                        "Couldn't get json from server. Check LogCat for possible errors!",
                        Toast.LENGTH_LONG).show());
            }

            return null;
        }
    }

    class StudentAdapter extends BaseAdapter {
        private HashMap<Integer, Student> items;

        public StudentAdapter(HashMap<Integer, Student> items) {
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("SetTextI18n")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.student_list_item, parent, false);
            TextView firstname = view.findViewById(R.id.first_name);
            TextView lastname = view.findViewById(R.id.last_name);
            TextView age = view.findViewById(R.id.age);
            TextView en_n = view.findViewById(R.id.en_no);
            TextView class_en = view.findViewById(R.id.student_class);
            firstname.setText("First Name: " + items.get(position).getFirstName().toUpperCase());
            lastname.setText("Last Name: " + items.get(position).getLastName().toUpperCase());
            age.setText("Age: " + items.get(position).getAge().toUpperCase());
            en_n.setText("Enroll_No: " + items.get(position).getEnrollmentNo().toUpperCase());
            class_en.setText("Class: " + items.get(position).getClassC().toUpperCase());
            return view;
        }
    }
}